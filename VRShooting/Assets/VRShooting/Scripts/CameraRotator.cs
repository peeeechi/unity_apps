﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    /// <summary>
    /// 回転速度
    /// </summary>
    [SerializeField]
    float angularVelocity = 30f;

    /// <summary>
    /// 水平方向の回転量
    /// </summary>
    float horizontalAngle = 0f;

    /// <summary>
    /// 垂直方向の回転量
    /// </summary>
    float verticalAngle = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var horizontalRotation = Input.GetAxis("Horizontal") * angularVelocity * Time.deltaTime;

        var verticalRotation = -Input.GetAxis("Vertical") * angularVelocity * Time.deltaTime;

        horizontalAngle += horizontalRotation;
        verticalAngle += verticalRotation;

        verticalAngle = Mathf.Clamp(verticalAngle, -80f, 80f);

        transform.rotation = Quaternion.Euler(verticalAngle, horizontalAngle, 0f);
    }
}
