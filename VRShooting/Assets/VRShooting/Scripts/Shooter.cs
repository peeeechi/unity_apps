﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    /// <summary>
    /// 弾のプレハブ
    /// </summary>
    [SerializeField]
    GameObject bulletPrehab;

    /// <summary>
    /// 銃口
    /// </summary>
    [SerializeField]
    Transform gunBarrelEnd;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))   // "Fire1" -> 左クリックや左Ctrl key の入力を取得するデフォルト定義
        {
            Shoot();
        }
    }

    void Shoot()
    {
        Instantiate(bulletPrehab, gunBarrelEnd.position, gunBarrelEnd.rotation);
    }
}
