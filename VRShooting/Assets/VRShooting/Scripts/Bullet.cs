﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{

    [SerializeField]
    /// <summary>
    /// 弾速 [m/s]
    /// </summary>
    float speed = 20f; 

    // Start is called before the first frame update
    void Start()
    {
        // ゲームオブジェクト前方の速度ベクトルを計算
        var velocity = speed * transform.forward;

        // Rigidbody コンポーネントを取得
        var rigidbody = GetComponent<Rigidbody>();

        // 前方方向へ初速を与える。
        rigidbody.AddForce(velocity, ForceMode.VelocityChange);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        // 衝突対象に"OnHitBullet"メッセージ
        other.SendMessage("OnHitBullet");

        // 自身のゲームオブジェクトを破棄
        Destroy(gameObject);
    }
}
